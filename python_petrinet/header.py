import numpy
import itertools
import sys, itertools
from bs4 import BeautifulSoup

class Miesto(object):

    def __init__(self, t):
        """
        :param t: pocet prechodov v sieti
        """
        self.konzum = numpy.array([0 for _ in range(t)])
        self.produce = numpy.array([0 for _ in range(t)])

    def pridaj_vstup(self, prechod, vaha):
        # z miesta do prechodu
        self.konzum[prechod] = vaha

    def pridaj_vystup(self, prechod, vaha):
        # z prechodu do miesta
        self.produce[prechod] = vaha

class Petri_siet(object):

    def __init__(self, p=0, t=0, siet=list()):
        """
        :param p: pocet miest
        :param t: pocet prechodov
        """
        self.p = p
        self.t = t
        self.siet = siet
        self.znackovanie = numpy.array([0 for m in range(self.p)])
        self.slova = list()
        self.vsetky_znackovania = set()
        self.graf = set()

    def nacitaj_data(self, nazov_suboru):
        """
        vrati list -> Miesto
        subor sa musi volat "petri.txt"
        """
        data = open(nazov_suboru, 'r')
        self.siet = []
        self.p, self.t = map(int, data.readline().strip().split())
        vstup, vystup = map(int, data.readline().strip().split())
        self.znackovanie = numpy.array(map(int, data.readline().strip().split()))

        for _ in range(self.p):
            self.siet.append(Miesto(self.t))

        #pridavanie vstupov
        for _ in range(vstup):
            miesto, prechod, hrana =  map(int, data.readline().strip().split())
            self.siet[miesto].pridaj_vstup(prechod, hrana)
        #pridavanie vystupov
        for _ in range(vystup):
            miesto, prechod, hrana = map(int, data.readline().strip().split())
            self.siet[miesto].pridaj_vystup(prechod, hrana)
        data.close()

    def pridaj_znacky(self, tokeny):
        """
        :param tokeny: pocet prvkov musi byt rovnaky ako pocet miest !!!
        :return: None
        """
        if len(self.siet) != len(tokeny):
            print 'fail pridaj_token'
            return
        self.znackovanie = numpy.array(tokeny)

    def vstupna_matica(self):
        return numpy.array([m.konzum for m in self.siet])

    def vystupna_matica(self):
        return numpy.array([m.produce for m in self.siet])

    def incidencna_matica(self):
        return self.vystupna_matica() - self.vstupna_matica()

    def print_vsetky_znackovania(self):
        print 'vsetky znackovania:'
        for x in self.vsetky_znackovania:
            print ', '.join('%0.f' % f for f in x)

    def scenare(self, max_len=1000):
        """
        funkcia nie je osetrena pre neohranicene siete
        :return: list so scenarmi
        """
        self.slova = list()
        self.vsetky_znackovania = set()
        self.vsetky_znackovania.add(tuple(self.znackovanie))
        self.graf = set()
        self.graf.add(('start', tuple(self.znackovanie)))
        for i in range(self.t):
            self.scenare_tmp(i, '', self.znackovanie, max_len)

    def scenare_tmp(self, prechod, slovo, tokeny, max_len):

        if len(slovo) >= max_len:
            return

        tokeny_pred = numpy.array(map(int, tokeny))
        tokeny = numpy.array(map(int, tokeny))

        vstupna = self.vstupna_matica().transpose()[prechod]
        vystupna = self.vystupna_matica().transpose()[prechod]

        if all(tokeny >= vstupna):

            tokeny -= vstupna
            tokeny += vystupna #po

            # obsluha slova
            slovo+= str(prechod)
            #print 'slovo:', slovo
            self.slova.append(slovo)
            self.vsetky_znackovania.add(tuple(tokeny))
            t = tuple(tokeny_pred), prechod, tuple(tokeny)
            self.graf.add(tuple(t))

            for i in range(self.t): #rekuzrivne volanie
                self.scenare_tmp(i, slovo, tokeny, max_len)

    def scenare_nespust(self, max_len):

        vsetky = []
        for i in range(1, max_len + 1):
            vsetky += [''.join(x) for x in itertools.product(map(str,range(self.t)), repeat=i)]

        self.scenare(max_len)
        nespust = set(vsetky) - set(self.slova)

        #print len(self.slova)
        #print len(vsetky)
        #print len(nespust)

        print 'nespustitelne scenare:'
        for x in nespust:
            print x

    def print_graf_dosiahnutelnosti(self):
        print 'graf dosiahnutelnosti:'
        for x in self.graf:
            print x

    def print_slova(self):
        print 'spustitelne slova:'
        for x in self.slova:
            print x


def nacitaj_petri_siet(filename):
    """
    :param filename: xml-subor s petriho sietov
    :return: instanciu Petri_siet
    """
    source_code = open(filename, 'r')
    soup = BeautifulSoup(source_code, "lxml")
    source_code.close()

    #print soup, '\n\n'

    tmp_p = soup.find_all('place')
    tmp_t = soup.find_all('transition')

    pocet_p = len(tmp_p)
    pocet_t = len(tmp_t)
    ide_tabulka = {} #musime ratat postupne, 0,1,2,...
    Petri = Petri_siet(p=pocet_p, t=pocet_t)
    Petri.siet = [None for _ in range(Petri.p)]
    #print '\nmiesta: %d'%pocet_p
    for i, p in enumerate(tmp_p):
        #print p, '\n'
        ide_p = int(p.find('id').get_text())
        label_p = str(p.find('label').get_text())
        tokens = int(p.find('tokens').get_text())
        #print ide_p, label_p, tokens
        ide_tabulka[ide_p] = i

        Petri.siet[i]=(Miesto(pocet_t))
        Petri.znackovanie[i] = tokens


    #print '\nprechody: %d'%pocet_t
    ide_tabulka_t = {}
    for i,t in enumerate(tmp_t):
        ide_t = int(t.find('id').get_text())
        label_t = str(t.find('label').get_text())
        #print ide_t, label_t
        ide_tabulka_t[ide_t] =i

    tmp_h = soup.find_all('arc')
    pocet_h = len(tmp_h)
    #print ide_tabulka, '\n', ide_tabulka_t
    #print '\nhrany: %d'%pocet_h
    for e in tmp_h:
        ide_h = int(e.find('id').get_text())
        source_ide = int(e.find('sourceid').get_text())
        desti_ide = int(e.find('destinationid').get_text())
        weight = int(e.find('multiplicity').get_text())
        #print ide_h, source_ide, distance_ide, weight

        if ide_tabulka.has_key(source_ide):
            i = ide_tabulka[source_ide]
            j = ide_tabulka_t[desti_ide]
            Petri.siet[i].pridaj_vstup(j, weight)
        else:
            i = ide_tabulka[desti_ide]
            j = ide_tabulka_t[source_ide]
            Petri.siet[i].pridaj_vystup(j, weight)

    return Petri

