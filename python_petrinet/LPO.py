from header import*

inf = sys.maxint
adj = {}  # [nd] -> element: (nd2, weight)


class Node(object):

    def __init__(self, nd, typ):

        self.nd = nd
        self.typ = typ
        self.places = list()  # pairs of (nd, weight)
        self.minulost_places = list() # to iste ako places len opacnym smerom

    def set_places(self, nd2, weight):
        self.places.append((nd2, weight))

    def set_minulost(self, nd2, weight):
        self.minulost_places.append((nd2, weight))

    def __str__(self):
        return `self.nd` + '\nplaces nasledujuce: ' + `self.places` +'\nplaces minulost: ' + `self.minulost_places`
        return `self.places`


class LPO_graf(object):

    def __init__(self, filename_petri, filename_lpo):
        self.miesta = dict() # {odkial: Node}
        self.minule_miesta = dict() #to iste ako miesta len opacnym smerom
        self.typy = dict()  # {cislo_stvorceka: typ_stvorceka}
        self.Petri = nacitaj_petri_siet(filename_petri)
        #self.nacitaj_data(filename_lpo)
        self.nacitaj_lpo_graf(filename_lpo)

    def nacitaj_data(self, filename):
        data = open(filename,'r')

        n, m, t = map(int,data.readline().strip().split(' '))
        self.n = n
        #n - pocet svorcekov, m- pocet riadkov(vstup), t- pocet typov prechodu
        for i in range(n):
            x, y = map(int, data.readline().strip().split(' '))
            self.typy[x] = y
        for a1 in range(m):
            x, y = map(int, data.readline().strip().split(' '))
            # initialization
            if not self.miesta.has_key(x):   #vytvorenie noveho Node
                nd = Node(x, self.typy[x])
                self.miesta[x] = nd
                self.minule_miesta[x] = nd
            if not self.miesta.has_key(y):
                nd = Node(y, self.typy[y])
                self.miesta[y] = nd
                self.minule_miesta[y] = nd
            self.miesta[x].set_places(y, 1)  #vytvorenie hrany
            #places[y].set_places(x, r)  #obojsmerne
            self.minule_miesta[y].set_minulost(x,1)
        data.close()

    def nacitaj_lpo_graf(self, filename):

        source_code = open(filename, 'r')
        soup = BeautifulSoup(source_code, "lxml")
        source_code.close()

        # print soup, '\n\n'

        tmp_p = soup.find_all('place')
        tmp_t = soup.find_all('transition')
        self.n = len(tmp_p) + len(tmp_t) #pocet stvorcekov

        ide_tabulka = {}
        label_tabulka = {'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6, 'h':7, 'i':8, 'j':9, 'k':10, 'l':11, 'm':12,
                         'n':13, 'o':14, 'p':15, 'q':16, 'r':17, 's':18, 't':19, 'u':20, 'v':21, 'w':22, 'x':23, 'y':24, 'z':25}
        i = 0

        for p in tmp_p + tmp_t:
            # print p, '\n'
            ide_p = int(p.find('id').get_text())
            label_p = str(p.find('label').get_text())
            # print ide_p, label_p, tokens
            ide_tabulka[ide_p] = i
            l = label_tabulka[label_p]
            nd = Node(i, l)
            self.miesta[i] = nd
            self.minule_miesta[i] = nd
            self.typy[i] = l
            i+=1

        tmp_h = soup.find_all('arc')
        pocet_h = len(tmp_h)
        #print ide_tabulka
        # print '\nhrany: %d'%pocet_h
        for e in tmp_h:
            ide_h = int(e.find('id').get_text())
            source_ide = int(e.find('sourceid').get_text())
            desti_ide = int(e.find('destinationid').get_text())
            weight = 1
            # print ide_h, source_ide, distance_ide, weight
            x = ide_tabulka[source_ide]
            y = ide_tabulka[desti_ide]
            self.miesta[x].set_places(y, 1)
            self.minule_miesta[y].set_minulost(x, 1)

    def pridaj_hranu(self, source_id, desti_id):
        x = source_id
        y = desti_id
        self.miesta[x].set_places(y, 1)
        self.minule_miesta[y].set_minulost(x, 1)

    def rezy(self, odkial, vrat=True):

        # vzdialenost - list s najkratsimi vzialenostami, kazdy index reprezentuje Node
        # pq - list pre iteraciu
        self.vzdialenost = [inf for _ in range(len(self.miesta))]
        self.vzdialenost[odkial] = 0
        pq = [(0, odkial)]  # (vaha hrany, node)

        while pq:
            it = pq.pop(0)
            u = it[1]
            for i in range(len(self.miesta[u].places)):
                v, weight = self.miesta[u].places[i]

                if self.vzdialenost[v] > self.vzdialenost[u] + weight:
                    self.vzdialenost[v] = self.vzdialenost[u] + weight
                    pq.append((self.vzdialenost[v], v))


        if vrat:return [i for i in range(len(self.vzdialenost)) if self.vzdialenost[i] == inf]
        else:   return [i for i in range(len(self.vzdialenost)) if 0 < self.vzdialenost[i] < inf]

    def tlac_rezy_tmp(self, odkial):
        self.rezy(odkial)
        print 'Node:  distance from source:', odkial
        for i in range(len(self.vzdialenost)):
            print "%s \t\t %d" % (str(i), self.vzdialenost[i])

    def vrat_rezy(self):
        #vrati vsetky rezy
        accepted = []
        combi = []
        for i in range(1, self.n+1):
            combi += [x for x in itertools.combinations(range(self.n),i)]
        for x in sorted(combi, key=len, reverse=True):
            acept = True #druha varianta ak neni 'break'
            if any([set(x).issubset(set(s[0])) for s in accepted]): #ak uz mame dlhsie slovo
                continue
            for i in x:
                kolizia = self.rezy(i, vrat=False)
                for k in kolizia:
                    if k in x:
                        break
                        acept = False
                else: continue
                break
            else:  # for-loop presiel cely
                minulost = set()
                for j in x:
                    for k in self.minulost(j):
                        minulost.add(k)
                accepted.append((x, list(minulost)))

        return  accepted

    def vrat_rezy_rovnice(self, vypis=False):
        accepted = self.vrat_rezy()
        for x in accepted:
            I = self.Petri.vstupna_matica()
            C = self.Petri.incidencna_matica()
            m0 = numpy.array(self.Petri.znackovanie)

            tmp_min = [0 for _ in range(self.Petri.t)]  #x[1]
            tmp_spust = [0 for _ in range(self.Petri.t)]  #x[0]
            for i in x[1]:
                tmp_min[self.typy[i]] +=1
            for i in x[0]:
                tmp_spust[self.typy[i]] +=1

            minulost = numpy.array(tmp_min)
            spustenie = numpy.array(tmp_spust)
            print 'spusetenie:',x[0],'minulost:',x[1], ':'
            if vypis:
                print m0, '+'
                print C, '*', minulost, '>='
                print I, '*', spustenie, ''
            print 'result:', m0 + C.dot(minulost) ,'>=', I.dot(spustenie), '\n'


    def minulost(self, odkial):

        # vzdialenost - list s najkratsimi vzialenostami, kazdy index reprezentuje Node
        # pq - list pre iteraciu

        self.vzdialenost = [inf for _ in range(len(self.minule_miesta))]
        self.vzdialenost[odkial] = 0
        pq = [(0, odkial)]  # (vaha hrany, node)

        while pq:
            it = pq.pop(0)
            u = it[1]
            for i in range(len(self.minule_miesta[u].minulost_places)):
                v, weight = self.minule_miesta[u].minulost_places[i]

                if self.vzdialenost[v] > self.vzdialenost[u] + weight:
                    self.vzdialenost[v] = self.vzdialenost[u] + weight
                    pq.append((self.vzdialenost[v], v))
        return [i for i in range(len(self.vzdialenost)) if 0 < self.vzdialenost[i] < inf]

    def print_minulost_tmp(self, odkial):
        self.minulost(odkial)
        print 'Node:  Minulost distance from source:', odkial
        for i in range(len(self.vzdialenost)):
            print "%s \t\t %d" % (str(i), self.vzdialenost[i])



filename_petri = sys.argv[1] #raw_input("zadaj subor petri_siet.xml")
filename_lpo = sys.argv[2] #raw_input("zadaj subor lpo_siet.xml")

graf = LPO_graf(filename_petri, filename_lpo)
#graf.pridaj_hranu(2, 5)



#graf.tlac_rezy_tmp(1)
#graf.rezy(5)
#print graf.rezy(1)
#print graf.minulost(5)
#graf.print_minulost_tmp(5)
#print graf.minule_miesta[5]
print "rezy:"
tmp = graf.vrat_rezy()
for x in tmp:
    print x

print '\n\n\n'
tmp = raw_input('chces rozsierny vypis? [y/n]')
tmp = True if tmp.lower() == 'y' else False
graf.vrat_rezy_rovnice(vypis=tmp)
