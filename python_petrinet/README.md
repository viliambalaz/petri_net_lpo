# Obsah
Tieto scripty majú poslúžiť na zápočtovú písomku z DUS.
- verzia: python 2.7
- moduly: numpy, bs4
- Unix:
- sudo apt-get install python-numpy
- sudo apt-get install python-bs4

- Windows
- download python27: [tu](https://www.python.org/downloads/release/python-2714/)
- v prikazovom riadku (ako spravca): 
- cd C:/python27
- cd Scripts pip.exe install nupmy
- pip.exe install bs4
- pip.exe install lxml 

## NET.py
Zo zadanej petriho siete, ktorá je predávana do programu ako .xml súbor vypíše vstupnú, výstupnú a incidenčnú maticu. Vypíše všetky spustitelné a nespustiteľné scenáre. Maximálna dĺžka slova je zadávana cez parameter. Program vypíše poradové čísla prechodov tj. slovo '03012' je 'adabc' Ďalej vypočíta všetky možné dosiahnuteľné značkovania a ktomu prislúchajúci graf dosiahnuteľnosti. (pre graficke zobrazenie grafu dosiahnuteľnosti môžete použiť simulator [apo](https://apo.adrian-jagusch.de/#/Sample%20Net).
Formát súboru: pri vytváraní petriho siete [tu](http://petriflow.com/) je prvé vytvorené miesto p1, druhé p2,... Rovnako to platí s prechodmi. Prvý prechod je t1 resp. a, druhý t2/b,... Poradové číslá prechodov začínajú od 0. Prvý vytvorený prechod má poradové číslo 0, druhý 1,... Miestam a prechodom netreba dávať názvy.

## LPO.py
Zo zadaného LPO grafu (xml súbor) a k nemu prislúchajúcu petriho sieť program vypíše maximálne rezy a ich minulosť a vypočíta nerovnosti.
Formát súboru: Miesta aj prechody označujú prechody, je to len z toho dôvodu, aby sa mohli pridávať hrany. Všetky miesta a prechody musia mať názov (a,b,c,d,e,f,...,z) tj. typ prechodu. 
Hrany je taktiež možné ručne pridať pomocou funkcie '.pridaj_hranu(x,y)', kde x je poradové číslo odkial a y kam. 

 - číslovanie: (od 0) najprv sa očíslujú miesta až potom prechody. Čiže prvé vytvorené miesto má poradové číslo 0, druhé miesto má č. 1 ,... Ak má LPO graf 6 miest, potom prvý vytvorený prechod má číslo 6, druhý prechod č.7, ...

## Príklad
V súbore je príklad s ktorým som pracoval a na ktorom mi to fungovalo. Celé to bolo robéne dosť narýchlo takže je možné, že sa nájdu chyby, ale verím že ako pomôcka pre kontrolu pomôže. 
